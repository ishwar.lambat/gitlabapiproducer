package com.gitlab.demo.gitlabapi;

import org.gitlab4j.api.GitLabApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class config {

    @Value("${gitlab.url}")
    private String gitLabUrl;

    @Value("${gitlab.token}")
    private String gitLabToken;

    @Bean
    public RestTemplate getRestTemplateBean(){
        return new RestTemplate();
    }

    @Bean
    public GitLabApi getGitApi(){
        GitLabApi gitLabApi = new GitLabApi(gitLabUrl, gitLabToken);

        return gitLabApi;
    }
}
