package com.gitlab.demo.gitlabapi.controller;

import com.gitlab.demo.gitlabapi.model.BranchModel;
import com.gitlab.demo.gitlabapi.model.MergeRequestBody;
import com.gitlab.demo.gitlabapi.service.GitLabAPIService;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GitLabAPIController {


    @Autowired
    private GitLabAPIService service;

    @GetMapping("/getMembers/{projectId}")
    List<Member> getMembersOfProject(@PathVariable("projectId") Integer projectId) throws GitLabApiException {
        List<Member> members = service.findMembersOfProject(projectId);
        return members;
    }

    @PostMapping("/createProject")
    public ResponseEntity<Project> addProject(@RequestParam("projectName") String projectName) throws GitLabApiException {
        Project result = service.createProject(projectName);

        return new ResponseEntity<Project>(result , HttpStatus.OK);
    }


    @GetMapping("/getBranches")
    public ResponseEntity<List<ProtectedBranch>> getGitAPI() throws GitLabApiException {

        List<ProtectedBranch> branches = service.getGitProjects();

        return new ResponseEntity<>(branches, HttpStatus.OK);
    }



    @PostMapping("/createBranch")
    public ResponseEntity<Branch> createBranch(@RequestBody BranchModel branchModel) throws GitLabApiException {

        Branch branch = service.createBranch(branchModel);

        return new ResponseEntity<>(branch, HttpStatus.CREATED);
    }

    @PostMapping("/createMerge")
    public ResponseEntity<MergeRequest> createMergeRequest(@RequestBody MergeRequestBody mergeRequestBody) throws GitLabApiException {
        return new ResponseEntity<MergeRequest>(service.generateMergeRequest(mergeRequestBody), HttpStatus.CREATED);
    }

}
