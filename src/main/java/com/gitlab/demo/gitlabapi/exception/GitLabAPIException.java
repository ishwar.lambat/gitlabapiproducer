package com.gitlab.demo.gitlabapi.exception;

import org.gitlab4j.api.GitLabApiException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@RestController
@ControllerAdvice
public class GitLabAPIException {

    @ExceptionHandler(value = GitLabApiException.class)
    public void exceptionGitLabApi(WebRequest request){

    }
}
