package com.gitlab.demo.gitlabapi.model;

public class MergeRequestBody {
    private Integer projectId;
    private String sourceBranch;
    private String targetBranch;
    private String title;
    private String desc;
    private  Integer assigneeId;

    public MergeRequestBody() {
    }

    public MergeRequestBody(Integer projectId, String sourceBranch, String targetBranch, String title, String desc, Integer assigneeId) {
        this.projectId = projectId;
        this.sourceBranch = sourceBranch;
        this.targetBranch = targetBranch;
        this.title = title;
        this.desc = desc;
        this.assigneeId = assigneeId;
    }



    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getSourceBranch() {
        return sourceBranch;
    }

    public void setSourceBranch(String sourceBranch) {
        this.sourceBranch = sourceBranch;
    }

    public String getTargetBranch() {
        return targetBranch;
    }

    public void setTargetBranch(String targetBranch) {
        this.targetBranch = targetBranch;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(Integer assigneeId) {
        this.assigneeId = assigneeId;
    }

    @Override
    public String toString() {
        return "MergeRequest{" +
                "projectId=" + projectId +
                ", sourceBranch='" + sourceBranch + '\'' +
                ", targetBranch='" + targetBranch + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
