package com.gitlab.demo.gitlabapi.service;

import com.gitlab.demo.gitlabapi.model.BranchModel;
import com.gitlab.demo.gitlabapi.model.MergeRequestBody;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GitLabAPIService {

    @Autowired
    GitLabApi gitLabApi;

    public List<Member> findMembersOfProject(Integer projectId) throws GitLabApiException{
        List<Member> members = gitLabApi.getProjectApi().getAllMembers(projectId);
        return members;
    }

    public List<ProtectedBranch> getGitProjects() throws GitLabApiException {


        List<ProtectedBranch> branches = gitLabApi.getProtectedBranchesApi().getProtectedBranches(32199034);
        //List<Project> projects = gitLabApi.getProjectApi().getProjects();
        return branches;
    }



    public Branch createBranch(BranchModel branchModel) throws GitLabApiException {
        Branch createdBranch = gitLabApi.getRepositoryApi().createBranch(branchModel.getProjectId(),
                branchModel.getBranchName(), branchModel.getBranchRef());
       // gitLabApi.getMergeRequestApi().getApprovalRules();
        return createdBranch;
    }

    public MergeRequest generateMergeRequest(MergeRequestBody mergeRequestBody) throws GitLabApiException {
       MergeRequest mergeRequestResult=gitLabApi.getMergeRequestApi()
               .createMergeRequest(mergeRequestBody.getProjectId(),mergeRequestBody.getSourceBranch(),
                       mergeRequestBody.getTargetBranch(),mergeRequestBody.getTitle(),mergeRequestBody.getDesc(),
                       mergeRequestBody.getAssigneeId());
        return mergeRequestResult;

    }

    public Project createProject(String projectName) throws GitLabApiException {
        Project project = gitLabApi.getProjectApi().createProject(projectName);

        return project;
    }



    /*@GetMapping("/myprojects")
    List<Project> getProjects() throws GitLabApiException {

        // Get the list of projects your account has access to
        List<Project> projects = gitLabApi.getProjectApi().getProjects(false, Visibility.PRIVATE, ProjectOrderBy.LAST_ACTIVITY_AT, SortOrder.ASC, "", false, true, true, false, false);
        return projects;

    }*/

}
